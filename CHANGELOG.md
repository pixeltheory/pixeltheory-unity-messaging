# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Unit tests
- Type checking for messages when generationg event extensions and control panel
- ulong (UInt64) skipping, as ulong serialized property property field sanity check is failing

## [2.0.0] - 2021.02.02
### CHANGED
- Changed MessagingSingletonBehaviour to take a generic type to be compatible with PixeltheorySingletonBehaviour..
- Reorganized layout of project files to more closely match Unity recommendations.
### FIXED
- Fixed missing UNITY_EDITOR compile flags for editor scripts.
- More formatting fixes for generated control panel script.

## [1.0.1] - 2019.09.07
### ADDED
- Added a messaging system compatible singleton (MessagingSingleBehaviour)

## [1.0.0] - 2019.08.11
### FIXED
- Fixed subclass parameter bug.
### ADDED
- Test interface for subclass parameter bug.

## [0.0.1] - 2019.07.23
### ADDED
- Added initial commit of messaging subsytem.
### CHANGED
- Updated README.md to reflect new instuctions on install and usage of messaging subsystem.

## [0.0.0] - 2019.06.16
### Added
- Added README.md file.
- Added CHANGELOG.md file.
- Added .gitattributes file.
- Added .gitignore file.
- Added Unity project.
- Added package.json.
